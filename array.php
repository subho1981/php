<?php
$data = [1,"tom",3,"max",["apple", "orange", "grapes"]];
//print_r($data);
//echo $data[4][0] . "\n";
echo $data[3] . "\n";

echo "----------associative_array---------- \n";
$ass_data = [
	"name" => "priya",
	"age" => 31,
	"address" => [
		"city" => "nagpur",
		"state" => "maharashtra",
		"pincode" => 441111,
	]
];
//print_r($ass_data);
//echo "$ass_data[name] \n";
echo $ass_data['address']['city'] . "\n";

$nums = [5,4,3,2,1];
$nums_len = sizeof($nums); 

echo "--------------printing numbers 1 to 10 using for loop ------------------------ \n";
for($i=0; $i<=$nums_len; ++$i){
	echo "$nums[$i] \n" ;
}

echo "-----------printing array values---------------- \n";
foreach($nums as $val){
	echo "$val \n";
}
?>
